package pce.marko.androidtablayout.chat;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import pce.marko.androidtablayout.R;

/**
 * Created by Davor on 6/23/2017.
 */

public class ChatActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
    }

}
