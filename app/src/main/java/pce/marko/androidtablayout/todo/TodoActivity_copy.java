package pce.marko.androidtablayout.todo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pce.marko.androidtablayout.db.DatabaseHandler;
import pce.marko.androidtablayout.R;
import pce.marko.androidtablayout.TodoItem;

public class TodoActivity_copy extends Activity {

    DatabaseHandler db;

    // -> private List<String> items;
    // -> private ArrayAdapter<String> itemsAdapter;
    private List<TodoItem> items;
    private ArrayAdapter<TodoItem> itemsAdapter;
    private ListView lvItems;

    Button btn;
    EditText edt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);
        // setContentView(R.layout.proba_todo);
        db = new DatabaseHandler(this);

        // lvItems = (ListView) findViewById(R.id.lista);
        // btn = (Button)findViewById(R.id.button);
        // edt = (EditText)findViewById(R.id.editText);

        lvItems = (ListView) findViewById(R.id.lvItems);
        btn = (Button)findViewById(R.id.btnAddItem);
        edt = (EditText)findViewById(R.id.etNewItem);


        items = new ArrayList<TodoItem>();
        // iza new treba napuniti items listu
        final List<TodoItem> listTodo = db.getAllTodos();
        for(TodoItem todoItem : listTodo) {
            items.add(todoItem);
        }

        // itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, items);
        itemsAdapter = new ArrayAdapter<>(this, R.layout.activity_todo_item, items);
        // itemsAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, items);
        lvItems.setAdapter(itemsAdapter);

        /* items.add("First item");
        items.add("Second item");
        itemsAdapter.add("Proba"); */
        // NA OBA OVA NACINA MOZES DODAVATI ITEM-E U LISTU, PREKO LISTE I PREKO ADAPTERA

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if(!edt.getText().toString().equals("")) {
                   String itemText = edt.getText().toString();
                   // itemsAdapter.add(itemText); // ArrayAdapter mozes punit direktno ili puneci listu items

                   db.addTodo(new TodoItem(itemText));
                   // items.add(itemText);
                   edt.setText("");
                   // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                   // InputMethodManager inputManager = (InputMethodManager) getActivity().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                   // inputManager.hideSoftInputFromWindow(getDialog().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                   //  edt.setInputType(InputType.TYPE_NULL);

                   InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                   inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
               }
            }
        });

        edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focused) {
               /* InputMethodManager keyboard = (InputMethodManager) getActivity(AndroidTabLayoutActivity).getSystemService(Context.INPUT_METHOD_SERVICE);
                if (focused)
                    keyboard.showSoftInput(edt, 0);
                else
                    keyboard.hideSoftInputFromWindow(edt.getWindowToken(), 0); */
            }
        });

        lvItems.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // return false;
                //// items.remove(position);
                // itemsAdapter.remove(position);
                // db.deleteTodo(position);
                // db.deleteTodo();
                //// -> itemsAdapter.notifyDataSetChanged();

                // itemsAdapter.getItemId(position);
                // String curID = itemsAdapter.getItem(position);

                // Log.d("ITEM ID ", String.valueOf(position) + ", " + String.valueOf(id) + ", " + curID);

                TextView pos = (TextView)view.findViewById(R.id.txtTitle);
                Log.d("ITEM pos ", pos.getText().toString());

                TodoItem todo = itemsAdapter.getItem(position);


                return true;
            }
        });

    }
}
