package pce.marko.androidtablayout.todo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.icu.text.UnicodeSetSpanner;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pce.marko.androidtablayout.AndroidTabLayoutActivity;
import pce.marko.androidtablayout.CustomBaseAdapter;
import pce.marko.androidtablayout.MojListAdapter;
import pce.marko.androidtablayout.db.DatabaseHandler;
import pce.marko.androidtablayout.R;
import pce.marko.androidtablayout.TodoItem;

import static android.app.PendingIntent.getActivity;

public class TodoActivity extends Activity implements View.OnClickListener {

    /* BAZA PODATAKA SE IZMEDJU VISE ACTIVITY-A MOZE CITATI I PISATI SAMO
       DEFINIRANJEM NOVIH INSTANCI OBJETKA BAZE PODATAKA. ZNACI SVAKI NOVI
       ACTIVITY MOZE PRISTUPITI BAZI DatabaseHandler I RADIT S NJOM PREKO
       INSTANCE DEFINIRANE U TOM NOVOM ACTIVITY-U DatabaseHandler db. */
    DatabaseHandler db;


    /* // TREBA RADIT SA KOMPLESNIM TIPOM PODATAKA TA LISTVIEW, A TO JE OBJEKT TodoItem,
       // KOJI SADRZI ID I STRING, A NE RADIT SAMO SA TIPOM STRING ZA PRIKAZ NA LISTVIEW.
       private List<String> items;
       private ArrayAdapter<String> itemsAdapter; */

    private List<TodoItem> items; // OVO JE DINAMICKA LISTA TODO ITEMA

    /* private ArrayAdapter<TodoItem> itemsAdapter;
       // PRIKAZ KOMPLEKSNOG TIPA TodoItem NE MOZE SE IZVESTI SA OBICNIM ARRAYADAPTEROM,
       // NEGO IPAK TREBA CUSTOM ARRAYADAPTER. */
    private MojListAdapter itemAdapter;
    // private CustomBaseAdapter itemsAdapter;
    // private ArrayAdapter<TodoItem> itemAdapters;

    // private ListView lvItems;
    private ListView listView;

    Button btn;
    EditText edt;


    @Override
    protected void onResume() {
        super.onResume();

        // BOLJE U onResume stavit citanje iz baze i prikaz na ListView
        // items = new ArrayList<TodoItem>();
        /* items = db.getAllTodos();
        adapter = new MojListAdapter(TodoActivity.this, R.layout.activity_todo_item, items);
        lvItems.setAdapter(adapter); */

    }

    @Override
    public void onClick(View view) {

        if(!edt.getText().toString().equals("") & btn.isPressed()) {
            // Toast.makeText(this, "onClick", Toast.LENGTH_SHORT).show();

            String itemText = edt.getText().toString();
            // itemsAdapter.add(itemText.toString()); // ArrayAdapter mozes punit direktno ili puneci listu items

             db.addTodo(new TodoItem(itemText));
             // -> items.add(new TodoItem(itemText)); // DODAVANJE ITEMA U LISTU.
             /* itemAdapter.add(new TodoItem(itemText)); // DODAVANJE ITEMA U ADAPTER
                itemAdapter.notifyDataSetChanged(); */ // KAD SE RADI NA OVAJ NACIN, DODANI ITEM PROCITAN IZ BAZE IMA INDEKS 0. TO NIJE DOBRO,
                                                       // JER SE TO NE MOZE IZBRISAT. TEK KAD SE NAPRAVI REFRESH, PROMJENOM SCREENA, IZ BAZE SE VIDI I TAJ RECORD.

             itemAdapter = new MojListAdapter(TodoActivity.this, R.layout.activity_todo_item, db.getAllTodos());
             listView.setAdapter(itemAdapter);

            edt.setText("");

            // MICANJE SISTEMSKE ANDROID TIPKOVNICE IZ EDIT TEXTA.
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        db = new DatabaseHandler(this);
        // lvItems = (ListView) findViewById(R.id.lvItems);
        listView = (ListView) findViewById(R.id.lvItems);
        btn = (Button)findViewById(R.id.btnAddItem);
        edt = (EditText)findViewById(R.id.etNewItem);



        /* final List<TodoItem> item = new ArrayList<>();
        item.add(new TodoItem("prvi"));
        item.add(new TodoItem("drugi"));
        item.add(new TodoItem("treci")); */
        items = db.getAllTodos();
        itemAdapter = new MojListAdapter(TodoActivity.this, R.layout.activity_todo_item, items);
        // itemAdapters = new ArrayAdapter<TodoItem>(TodoActivity.this, R.layout.activity_todo_item, items);
        listView.setAdapter(itemAdapter);

        btn.setOnClickListener(this);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                  // items.remove(i);

                  // itemAdapter.notifyDataSetChanged();
                  TodoItem todopos = (TodoItem) adapterView.getItemAtPosition(i);
                  TodoItem listTodopos = (TodoItem) listView.getAdapter().getItem(i); // OVA I GORNJA LINIJA SU ISTE, BOLJE PREKO ADAPTERVIEW, JER JE TO I KLIKNUTO
                  //// TodoItem todopos = (TodoItem) listView.getAdapter().getItem(i);
                   db.deleteTodo(todopos.getId());
                   itemAdapter.remove(listTodopos); // OVO !!! IZ ADAPTERA listView-a IZBRISI ONAJ ITEM KOJI SADRZI <TodoItem> listTodopos == todopos - RJESENJE
                   // DO SADA BI BRISAO ITEM ZA List<>, ALI MOZES TO I TAKO DA PREKO ARRAYADAPTERA<>() IZBRISES ITEM KOJI SADRZI Object TodoItem.
                //   itemAdapter.notifyDataSetChanged(); - POSTO notify() NIJE UKLJUCEN, TKO PONOVNO PRESLOZI ID IZ DB SA INDEXOM LISTVIEW.
                                                           // DA LI itemAdapter.remove() TO AUTOMATSKI ODRADI, PRIJE NIJE TAKO RADILO !?
                  /* RJESENJE KAKO VEZATI BRISANJE LIST VIEW ITEM INDEX I SQLITE DB RECORD.
                    (TREBA PROUCITI NA�IN SA DAVANJEM ISTOG TAG-A LIST VIEW-U I SQLITE RECORDU, TAKO KAD SE BRISE RECORD SA SVOJIM ID-EM
                     I OZNACENIM TAG-OM, BRISE SE LIST VIEW SA SVOJIM TAG-OM, ALI I ISTIM TAGOM KAO I DB RECORD.)
                     db.deleteTodo(todopos.getId()); - BRISE RECORD, REDAK U DB INDEKSA ID KOJI JE U OBJEKTU todopos.
                     itemAdapter.remove(listTodopos); - BRISE ITEM IZ ARRAY ADAPTERA itemAdapter, ALI BRISE ITEM NA OSNOVU OBJEKTA TodoItem
                                                        KOJEG TREBA IZBRISATI, A NE NA OSNOVU INDEXA LIST VIEW KOJI JE VEZAN NA ARRAY ADAPTER. */
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                TodoItem listTodopos = (TodoItem) listView.getAdapter().getItem(i); // OVA I GORNJA LINIJA SU ISTE, BOLJE PREKO ADAPTERVIEW, JER JE TO I KLIKNUTO
                final TodoItem todopos = (TodoItem) listView.getAdapter().getItem(i);
                Log.d("itemClick ",  todopos.getId() + " : " + listTodopos + " : " + i);

                AlertDialog.Builder alertbox = new AlertDialog.Builder(TodoActivity.this);
                        alertbox.setTitle("Update");

                // DINAMICKI EDITTEXT
                final EditText input = new EditText(TodoActivity.this);
                final String tekst = todopos.getTodo().toString();

                input.setText(tekst);
                // input.setInputType(InputType.TYPE_CLASS_TEXT);
                // Postavljanje teksta u novi red
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                input.setLines(5);
                input.setMaxLines(5);
                input.setGravity(Gravity.LEFT | Gravity.TOP);

                alertbox.setView(input);

                alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TodoItem itemTodo = new TodoItem(todopos.getId(), input.getText().toString() );
                        db.updateTodo(itemTodo);

                        itemAdapter = new MojListAdapter(TodoActivity.this, R.layout.activity_todo_item, db.getAllTodos());
                        listView.setAdapter(itemAdapter);
                    }
                })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                alertbox.show();


               // db.deleteTodo(todopos.getId());
               // itemAdapter.remove(listTodopos);

            }
        });

    }

}
