package pce.marko.androidtablayout;

/**
 * Created by Davor on 6/28/2017.
 */

public class TodoItem {

    int id;
    String todo;

    public TodoItem() {

    }

    public TodoItem(String todo) {
        this.todo = todo;
    }

    public TodoItem(int id, String todo) {
        this.id = id;
        this.todo = todo;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getTodo(){
        return todo;
    }

    public void setTodo(String todo){
        this.todo = todo;
    }

}
