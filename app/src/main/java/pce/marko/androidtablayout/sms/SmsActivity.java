package pce.marko.androidtablayout.sms;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pce.marko.androidtablayout.R;

// public class SmsActivity extends Activity {
public class SmsActivity extends ListActivity {
              // KADA STAVIS ListActivity, U layout MORA BITI FILE KOJI IMA DEFINIRAN @android:id/list -> ListView android:id="@android:id/list"

    List<SMSData> smsLista = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        /* Cursor c = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        startManagingCursor(c);

        if(c.moveToFirst()){
            do{
                SMSData smsData = new SMSData();
                smsData.setNumber(c.getString(c.getColumnIndexOrThrow("address")));
                smsData.setNumber(c.getString(c.getColumnIndexOrThrow("body")));
                smsLista.add(smsData);

            }while(c.moveToNext());
        }

        String[] from = new String[]{"address", "body"};
        int[] to = new int[]{R.id.number_entry, R.id.name_entry};
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.activity_sms_list, c, from, to);
        setListAdapter(adapter); */
    }

   @Override
    protected void onResume(){
        super.onResume();

         Cursor c = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
       // StringBuilder sb = new StringBuilder();
       startManagingCursor(c);

       if(c.moveToFirst()){
           do{
               SMSData smsData = new SMSData();
               smsData.setNumber(c.getString(c.getColumnIndexOrThrow("address")));
               smsData.setBody(c.getString(c.getColumnIndexOrThrow("body"))); // !
               smsLista.add(smsData);
           }while (c.moveToNext());
       }


       String[] from = new String[]{"address", "body"};
        int[] to = new int[]{R.id.number_entry, R.id.name_entry};
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.activity_sms_list, c, from, to);
        setListAdapter(adapter);
    }


   @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
          // super.onListItemClick(l, v, position, id);
        String Body = smsLista.get(position).getBody();

        Toast.makeText(this, "SMS " + String.valueOf(position) + "\n " + Body, Toast.LENGTH_SHORT).show();

        for(int idx = 0; idx < smsLista.size(); idx++){
            Log.d("smsLista ", smsLista.get(position).getNumber() );
            Log.d("smsLista ", smsLista.get(position).getBody() );
        }

        // Intent intent = new Intent(this, ReadActivity.class);
        // intent.putExtra("id", String.valueOf(position));
        // intent.putExtra("broj", smsLista.get(position).getNumber());
        // intent.putExtra("body", smsLista.get(position).getBody());
        // startActivity(intent);


    }


}
