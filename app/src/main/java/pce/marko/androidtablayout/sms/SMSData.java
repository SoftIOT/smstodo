package pce.marko.androidtablayout.sms;

/**
 * Created by Davor on 6/23/2017.
 */

public class SMSData{

        private String number;
        private String body;

        public String getNumber(){
            return number;
        }

        public void setNumber(String number){
            this.number = number;
        }

        public String getBody(){
            return  body;
        }

        public void setBody(String body){
            this.body = body;
        }

}
