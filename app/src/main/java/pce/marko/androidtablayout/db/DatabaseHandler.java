package pce.marko.androidtablayout.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import pce.marko.androidtablayout.TodoItem;

/**
 * Created by Davor on 6/28/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "todo_db";
    private static final String DATABASE_TABLE = "todo_table";

    private static final String KEY_ID = "id";
    private static final String KEY_TODO = "todo";


    // public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TODO_TABLE = "CREATE TABLE " + DATABASE_TABLE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TODO + " TEXT" + ");";
                                   // "CREATE TABLE todo_table (id INTEGER PRIMARY KEY NOT NULL, todo TEXT NOT NULL);" // Na kraju obavezno ;
        db.execSQL(CREATE_TODO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);

        // Create tables again
        onCreate(db);
    }

    public void addTodo(TodoItem todoItem){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_TODO, todoItem.getTodo());

        db.insert(DATABASE_TABLE, null, contentValues);
        // db.insertWithOnConflict(DATABASE_TABLE, null, contentValues, null, null, null, null);
        db.close();
    }

    public List<TodoItem> getAllTodos(){

        List<TodoItem> todoLista = new ArrayList<TodoItem>();
        String upit = "SELECT * FROM " + DATABASE_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(upit, null);

         if(cursor.moveToFirst()){
             do {
                 TodoItem todoItem = new TodoItem();
                 todoItem.setId(Integer.parseInt(cursor.getString(0))); // MORAS I id IZVUC IZ TABLICE DA BI TO ZAJEDNO SA STRINGOM MOGAO POKAZAT.
                 todoItem.setTodo(cursor.getString(1));
                // String todoItem =
                 todoLista.add(todoItem);
             }while(cursor.moveToNext());
         }

        db.close();
        return todoLista;
    }

    //  public List<TodoItem> deleteTodo(int id){
    public void deleteTodo(int id){
        // List<TodoItem> retList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(DATABASE_TABLE, KEY_ID + " = ?", new String[]{String.valueOf(id)});
        Log.d("DELETE ", String.valueOf(id));

        db.close();

         // return getAllTodos();
    }

    public void updateTodo(TodoItem item){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues content = new ContentValues();
        content.put(KEY_TODO, item.getTodo());

        Log.d("UPDATE ", String.valueOf(item.getId()) + " . " + item.getTodo());

        db.update(DATABASE_TABLE, content, KEY_ID + " = ? ", new String[]{String.valueOf(item.getId())});
    }

    public int getTodoCounts() {
        String countQuery = "SELECT  * FROM " + DATABASE_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
        /* String countTodo = "SELECT * FROM " + DATABASE_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cur = db.rawQuery(countTodo, null);
        int result = cur.getCount();

        cur.close();
        db.close();
        return result; */
    }

}
