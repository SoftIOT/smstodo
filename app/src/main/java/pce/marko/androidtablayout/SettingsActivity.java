package pce.marko.androidtablayout;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.Log;

/**
 * Created by Davor on 6/23/2017.
 */

public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference_settings);

        // RADI I KAD SE PREFERENCE ACTIVITY REGISTRIRA U onCreate().
        // ALI BOLJE OSTAVI TO U onResume() REGISTRACIJU I OBAVEZNO UNREGISTER U onPause() ZBOG GARBAGE COLLECTORA.
        //// PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        // U Resume() registriras se na PreferenceActivity ChangeListener
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d("SharedPrefChanged ", key);

        Preference pref = findPreference(key);

        if(pref instanceof ListPreference){
            ListPreference listPref = (ListPreference) pref;
            pref.setSummary(listPref.getSummary());
            return;
        }

    }
}

