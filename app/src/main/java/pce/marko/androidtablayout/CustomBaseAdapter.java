package pce.marko.androidtablayout;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pce.marko.androidtablayout.todo.TodoActivity;

/**
 * Created by Davor on 7/6/2017.
 */

public class CustomBaseAdapter extends BaseAdapter{

    Context context;

    List<TodoItem> todoLista;

    /* ArrayList<TodoItem> TodoList;
    public CustomBaseAdapter(Context context, ArrayList<TodoItem> list){
        this.context = context;
        TodoList = list;
    } */

    public CustomBaseAdapter(Context context, List<TodoItem> lista){
        this.context = context;
        todoLista = lista;
    }


    @Override
    public int getCount() {
        return todoLista.size();
    }

    @Override
    public Object getItem(int i) {
        return todoLista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        TodoItem TodoItemList = todoLista.get(i);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_todo_item, null);
        }

        TextView todoText = (TextView)convertView.findViewById(R.id.txtTitle);
        todoText.setText(todoLista.get(i).getTodo());
        // todoText.setText(TodoItemList.getTodo());
        // notifyDataSetChanged();
        Log.d("Base Adapter ",  String.valueOf(getCount()));

        return convertView;
    }
}
