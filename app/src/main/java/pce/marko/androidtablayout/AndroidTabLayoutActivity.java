package pce.marko.androidtablayout;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.Toast;

import java.util.List;

import pce.marko.androidtablayout.alarm.AlarmActivity;
import pce.marko.androidtablayout.chat.ChatActivity;
import pce.marko.androidtablayout.db.DatabaseHandler;
import pce.marko.androidtablayout.sms.SmsActivity;
import pce.marko.androidtablayout.todo.TodoActivity;

public class AndroidTabLayoutActivity extends TabActivity implements TabHost.OnTabChangeListener{

    DatabaseHandler db = new DatabaseHandler(this);
    TabHost tabHost; // = getTabHost();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_tab_layout);

        tabHost = getTabHost();

        // Tab for todo
        TabHost.TabSpec todospec = tabHost.newTabSpec("Todo");
        // setting Title and Icon for the Tab
        todospec.setIndicator("TODO");

        // tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#4E4E9C"));
        Intent todoIntent = new Intent(this, TodoActivity.class);
        todospec.setContent(todoIntent);

        // Tab for sms
        TabHost.TabSpec smsspec = tabHost.newTabSpec("Sms");
        smsspec.setIndicator("SMS");
        Intent smsIntent = new Intent(this, SmsActivity.class);
        smsspec.setContent(smsIntent);

        // Tab for alarm
        TabHost.TabSpec alarmspec = tabHost.newTabSpec("Alarm");
        alarmspec.setIndicator("ALARM");
        Intent alarmIntent = new Intent(this, AlarmActivity.class);
        alarmspec.setContent(alarmIntent);


        // Tab for chat
        TabHost.TabSpec chatspec = tabHost.newTabSpec("Chat");
        chatspec.setIndicator("CHAT");
        Intent chatIntent = new Intent(this, ChatActivity.class);
        chatspec.setContent(chatIntent);

        tabHost.addTab(todospec); // Adding todo tab
        tabHost.addTab(smsspec); // Adding smstab
        tabHost.addTab(alarmspec); // Adding alarmtab
        tabHost.addTab(chatspec); // Adding alarmtab

        /* for(int i=0; i<tabHost.getTabWidget().getChildCount(); i++)
        {
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#8A4117"));
        }
          tabHost.getTabWidget().setCurrentTab(1);
          tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#C35817")); */

        for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)
        {
            // tabHost.getTabWidget().getChildAt(i).setBackgroundColor(color.);
            // tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.BLUE);
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#ADD8E6"));
        }
        // tabHost.getTabWidget().setCurrentTab(0);
        // tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.WHITE);


        /* // DatabaseHandler db = new DatabaseHandler(this);
        try {

            //  Log.d("Insert ", "DATA");
            // db.addTodo(new TodoItem("Probaj ..."));
            // db.addTodo(new TodoItem("Kupi "));
            // db.addTodo(new TodoItem("Vidi "));

            Log.d("procitaj ", "DATA");
            List<TodoItem> lista = db.getAllTodos();
            for (TodoItem todoItem : lista) {
                Log.d("ITEM:", todoItem.getId() + ", " + todoItem.getTodo());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            // db.close();
        } */

    }

    @Override
    public void onTabChanged(String s) {
        /* for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)
        {
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#8A4117"));
        }

        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.parseColor("#C35817")); */

        /* for(int i=0;i<tabHost.getTabWidget().getChildCount();i++)
        {
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.GREEN);
        }

        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(Color.WHITE); */
    }

    @Override
    protected void onResume() {
        super.onResume();

        for (TodoItem todoItem : db.getAllTodos()) {
            Log.d("ITEM: id", todoItem.id + ", " + todoItem.getTodo());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /* if (id == R.id.action_settings) {
            return true;
        } */

        switch(item.getItemId()){
            case R.id.home:
                // Toast.makeText(this, "Home", Toast.LENGTH_LONG).show();
                // db.getAllTodos();
                // Log.d("procitaj ", "DATA");
                List<TodoItem> lista = db.getAllTodos();
                Log.d("ITEM: id", " --------------------------- ");
                for (TodoItem todoItem : lista) {
                    Log.d("ITEM: id", todoItem.getTodo() + ", " + todoItem.id);
                }
                break;
            case R.id.about:
                break;
            case R.id.settings:
                 Intent intent = new Intent(AndroidTabLayoutActivity.this, SettingsActivity.class);
                 startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}
