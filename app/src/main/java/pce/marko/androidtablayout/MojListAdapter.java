package pce.marko.androidtablayout;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import pce.marko.androidtablayout.db.DatabaseHandler;

/**
 * Created by Davor on 6/30/2017.
 */

public class MojListAdapter extends ArrayAdapter<TodoItem> {

    Context context;
    int layoutResourceId;
    List<TodoItem> todoList;
    // DatabaseHandler db;


    // public MojListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<TodoItem> todoItemList, DatabaseHandler db) {
    public MojListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<TodoItem> todoItemList) {
        super(context, resource, todoItemList);
        this.context = context;
        this.layoutResourceId = resource;
        this.todoList = todoItemList;
        // this.db = db;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // View rowView = inflater.inflate(R.layout.activity_todo, parent, false);
        View rowView = inflater.inflate(R.layout.activity_todo_item, parent, false);
        /* OVDJE SE inflate() LAYOUT activity_todo_item, JER SE U NJEMU NALAZI ITEM U KOJI CE SE UPISATI SADRZAJ */

        /* ITEM id.txtTitle IZ activity_todo_item LAYOUT-A U KOJI SE PISE SADRZAJ ZA LISTVIEW */
        TextView todoText = (TextView)rowView.findViewById(R.id.txtTitle);
        todoText.setText(todoList.get(position).getTodo());

        /* InputFilter[] FilterArr = new InputFilter[1];
        FilterArr[0] = new InputFilter.LengthFilter(30);
        todoText.setFilters(FilterArr); */


        // -> Log.d("Item CustomAdapter ", todoList.get(position).getId() + " : " + todoList.get(position).getTodo());

        /* for (TodoItem item : db.getAllTodos()) {
            Log.d("Item db ", item.getId() + ":" + item.getTodo());
        } */

        //// rowView.setTag(todoList.get(position).getId());
        //// todoText.setTag(todoList.get(position).getId());
        // notifyDataSetChanged(); //!

        return rowView;
    }
}
